# Projecte-IvanPardo

# Repte 2

Aquest és el Repte 2  En aquesta guia, es descriu com configurar una imatge de Docker amb les especificacions requerides i com comprovar que està configurada correctament.

## Configuració del Dockerfile

El primer pas és crear un Dockerfile amb la configuració següent:

```Dockerfile
FROM debian:latest
LABEL author="Ivan Pardo"
LABEL subject="Repte 2"
RUN apt-get update
RUN apt-get install -y procps iproute2 nmap tree vim
RUN mkdir /opt/Repte2
COPY * /opt/Repte2
RUN chmod +x /opt/Repte2/startup.sh
WORKDIR /opt/Repte2
CMD /opt/Repte2/starupt.sh
EXPOSE 80

## Construir la maquina virtual
docker build -t a220877ipm14/repte2:detach .

## Comprobar que tot funcioni (cal posar la -p per obrir els ports)
docker run --rm --name Repte1 -h edt.org -p 80:80 a220877ipm14/repte2:detach

