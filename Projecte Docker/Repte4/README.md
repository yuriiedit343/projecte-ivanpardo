# Repte 4-IvanPardo

El primer pas seria crear un Dockerfile amb el següent contingut:

```Dockerfile
# Repte 4 Dockerfile
FROM debian:latest
LABEL version="1.0"
LABEL author="IvanPardo"
LABEL subject="Repte4"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd 
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```
El segon pas es crear l'arxiu startup.sh amb aquest codi:
```
#!/bin/bash

# export DEBIAN_FRONTEND=noninteractive
# apt-get -y install slapd
echo "Iniciant configuració LDAP..."
#rm -rf /etc/ldap/slapd.d/*
#rm -rf /var/lib/ldap/*
#slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
#slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

#chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
```
creem l'arxiu slapd.conf que contindra la configuracio del slapd
```
#
# Veure slapd.conf(5) per detalls sobre les opcions de configuració.
# Aquest arxiu NO hauria de ser llegible per a tothom.
#
# Paquets Debian: slapd ldap-utils

include         /etc/ldap/schema/corba.schema
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/duaconf.schema
include         /etc/ldap/schema/dyngroup.schema
include         /etc/ldap/schema/inetorgperson.schema
include         /etc/ldap/schema/java.schema
include         /etc/ldap/schema/misc.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/openldap.schema
#include                /etc/ldap/schema/ppolicy.schema
include         /etc/ldap/schema/collective.schema

# Permetre connexions de clients LDAPv2. Això NO és la configuració per defecte.
allow bind_v2

pidfile         /var/run/slapd/slapd.pid
#argsfile       /var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la
# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```
*Despres tindriem que posar el fitxer amb totes les dades de la database a la nostra carpeta abans de fer l'imatge, en el meu cas l'arxiu es dira edt-org.ldif*
I creem la imatge amb la comanda:
```
docker build -t ldaprepte4:base .
```

Per iniciar la maquina seria:
```
docker run --rm --name repte4ivanpardo -v volume1:/var/tmp/projecte-ivanpardo/Projecte\ Docker/Repte4 -h  ldap.edt.org  -p 389:389 -d ldaprepte4:base
```
I un cop dins executariem el startup.sh
