# Repte 7-IvanPardo

El primer pas seria crear un Dockerfile amb el següent contingut:

```Dockerfile
# SSH server exemple detach amb Debian
FROM debian
LABEL subject="SSH server"
LABEL author="ivanPardo"
RUN apt-get update && apt-get -y install openssh-client openssh-server
COPY useradd.sh /tmp
RUN bash /tmp/useradd.sh
WORKDIR /tmp
CMD /etc/init.d/ssh start -D
EXPOSE 22

```
El segon pas es crear l'arxiu useradd.sh amb aquest codi: (el que fa es afegir usuaris pre fabricats per poder iniciar sesio)
```
#! /bin/bash
# Creació d'usuaris prefixats per verificar accés SSH
for user in pere marta anna pau jordi julia
do
useradd -m $user
echo $user:$user | chpasswd
done

```

I creem la imatge amb la comanda:
```
docker build -t ldaprepte7ssh:latest .
```

Per iniciar la maquina seria:
```
docker run --rm --name repte7ssh/ivanpardo  -h  ldap.edt.org  -p 2222:22  -d ldaprepte7ssh:latest
```
per comprovar el funcionament correcte dins de la maquina hauriem de fer:
```
ssh -p 2222 unit01@localhost
```
