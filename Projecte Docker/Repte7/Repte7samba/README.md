# Repte 7-IvanPardo

El primer pas seria crear un Dockerfile amb el següent contingut:

```Dockerfile
FROM debian:latest
LABEL version="1.0"
LABEL author="Ivan Pardo"
LABEL subject="Repte7 samba"
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install procps samba smbclient
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```
El segon pas es crear l'arxiu startup.sh on crearem la xixa i crearem un usuari
```
#! /bin/bash
# @edt ASIX M06 2019-2020
# startup.sh
# -------------------------------------
# Creació de xixa per als shares
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.
mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.
# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf
121
# Creació usuaris unix/samba
useradd -m -s /bin/bash lila
useradd -m -s /bin/bash roc
useradd -m -s /bin/bash patipla
useradd -m -s /bin/bash pla
echo -e "lila\nlila" | smbpasswd -a lila
echo -e "roc\nroc" | smbpasswd -a roc
echo -e "patipla\npatipla" | smbpasswd -a patipla
echo -e "pla\npla" | smbpasswd -a pla
# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb Ok"

```

Copiarem l'arxiu smb.conf amb la configuracio que nosaltres volguem
```
[global]
workgroup = MYGROUP
server string = Samba Server Version %v
log file = /var/log/samba/log.%m
max log size = 50
security = user
passdb backend = tdbsam
load printers = yes
cups options = raw
[homes]
comment = Home Directories
browseable = no
writable = yes
; valid users = %S
; valid users = MYDOMAIN\%S
[printers]
comment = All Printers
path = /var/spool/samba
browseable = no
guest ok = no
writable = no
printable = yes
[documentation]
comment = Documentació doc del container
path = /usr/share/doc
public = yes
browseable = yes
writable = no
printable = no
guest ok = yes
[manpages]
comment = Documentació man del container
path = /usr/share/man
public = yes
browseable = yes
writable = no
printable = no
guest ok = yes
[public]
comment = Share de contingut public
path = /var/lib/samba/public
public = yes
browseable = yes
writable = yes
printable = no
guest ok = yes
[privat]
comment = Share d'accés privat
path = /var/lib/samba/privat
public = no
browseable = no
writable = yes
printable = no
guest ok = yes
```

Per iniciar la maquina seria:
```
docker run --rm --name repte7samba/ivanpardo  -h  ldap.edt.org  -p 139:139 -p 445:445 -d ldaprepte7samba:latest
```
per comprovar el funcionament correcte dins de la maquina hauriem de fer:
```
smbclient //172.17.0.2/public
smblcient //localhost/public
```
