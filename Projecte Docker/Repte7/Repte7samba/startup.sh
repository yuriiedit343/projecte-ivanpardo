#! /bin/bash
# @edt ASIX M06 2019-2020
# startup.sh
# -------------------------------------
# Creació de xixa per als shares
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.
mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.
# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf
# Creació usuaris unix/samba
useradd -m -s /bin/bash pepe
echo -e "pepe\npepe" | smbpasswd -a pepe
# Activar els serveis
/usr/sbin/smbd
/usr/sbin/nmbd -F 
