# Repte 7-IvanPardo

El primer pas seria crear un Dockerfile amb el següent contingut:

```Dockerfile
# Repte 7 Dockerfile
# net21:base
FROM debian
LABEL author="Ivan Pardo"
LABEL description="repte7 part net"
RUN apt-get update && apt-get -y install xinetd iproute2 iputils-ping nmap procps net-tools vsftpd apache2 tftpd-hpa
COPY daytime-stream chargen-stream echo-stream /etc/xinetd.d/
COPY * /tmp
RUN chmod -x /tmp/startup.sh
WORKDIR /tmp
CMD [ "bash /tmp/startup.sh" ]
EXPOSE 7 13 19

```
El segon pas es crear l'arxiu startup.sh amb aquest codi:
```
#!/bin/bash
cp /tmp/index.html /var/www/html/index.html

/etc/init.d/vsftpd start

/etc/init.d/tftpd-hpa start

apache2ctl -D0

/usr/sbin/xinetd -dontfork

```
Copiem els 3 arxius necessesaris per al daytime charge i echo
I afegim l'arxiu index.html 

I creem la imatge amb la comanda:
```
docker build -t repte7net:latest .
```

Per iniciar la maquina seria:
```
docker run --rm --name repte7/ivanpardo  -h  ldap.edt.org  -p 7:7 -p 13:13 -p 19:19 -p 80:80  -it ldaprepte6:latest /bin/bash
```
per comprovar el funcionament correcte dins de la maquina hauriem de fer:
```
nc localhost 13
nc localhost 19
nc localhost 7
buscar en google localhost per comprobar l'apache
```
