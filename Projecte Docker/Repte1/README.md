# Projecte-IvanPardo

# Repte 1

Aquest és el Repte 1  En aquesta guia, es descriu com configurar una imatge de Docker amb les especificacions requerides i com comprovar que està configurada correctament.

## Configuració del Dockerfile

El primer pas és crear un Dockerfile amb la configuració següent:

```Dockerfile
FROM debian:latest
LABEL author="Ivan Pardo"
LABEL subject="Repte 1"
RUN apt-get update
RUN apt-get install -y procps iproute2 nmap tree vim
RUN mkdir /opt/Repte1
WORKDIR /opt/Repte1


## Construir la maquina virtual
docker build -t a220877ipm14/repte1:base .

## Comprobar que tot funcioni
docker run --rm --name Repte1 -h edt.org a220877ipm14/repte1:base tree /

