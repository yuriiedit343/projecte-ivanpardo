## Repte 3--IvanPardo

### Creacio de volum
El primer que hem de crear sera el volum amb la comanda ```docker volume create volum1``` on volum1 es el nom que volem utilitzar

### Instalacio imatge postgres
Per instalar la imatge de postgres hem de fer ```docker pull postgres```

### Afegir xixa
Per poder entrar a una base de dades feta, el que hem de fer es posar un scipt de sql a la nostra carpeta abans de fer el docker build, per aixis crear la base de dades

### Dockerfile
En l'arxiu Dockerfile l'unic que haurem de fer es afegir que es copii el nostre script a la carpeta que s'anomena /docker-entrypoint-initdb.d/
```
FROM postgres:latest
copy dades.sql /docker-entrypoint-initdb.d/
```
### Inici Maquina virtual
Per iniciar la maquina assignant amb el volum en la carpeta on volem posar les dades, indicar la contrasenya del postgres, obrir el port i posar la maquina en detach es : ```docker run --rm -v volum1:/dades --name mi-postgres -e POSTGRES_PASSWORD=secret -p5432:5432 -d postgres:latest```

### Comprovacio
Per entrar a la maquina virtual i veure que funciona : ```docker exect -it mi-postgres /bin/bash```
un cop dins, haurem de posar les comandes, ```su postgres``` i ```psql```
