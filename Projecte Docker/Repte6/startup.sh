#!/bin/bash
echo "Configurant el servidor ldap..."

sed -i "s/Manager/$LDAP_ADMIN_USERNAME/g" slapd.conf
sed -i "s/secret/$LDAP_ADMIN_PASSWORD/g" slapd.conf
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
 
