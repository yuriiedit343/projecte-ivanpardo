#!/bin/bash

if [ "$#" -eq 0 ]; then
  option="start"
else
  option="$1"
fi
case "$option" in
  "slapd")
    # Crear un servidor LDAP vacío
    echo "Creando un servidor LDAP vacío..."
    rm -rf /var/lib/ldap/*
    rm -rf /etc/ldap/slapd.d/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "initdb")
    # Inicializar el servidor LDAP con datos de la organización
    echo "Inicializando el servidor LDAP con datos de la organización..."
    /usr/sbin/slapd -d0
    ;;
  "start" | "edtorg" | "-z $1")
    # Iniciar el servidor LDAP utilizando la persistencia de datos de la base de datos y la configuración.
    echo "Iniciando el servidor LDAP utilizando la persistencia de datos existentes..."
    rm -rf /var/lib/ldap/*
    rm -rf /etc/ldap/slapd.d/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0

    ;;
  "slapcat")
	  if [ -n "$2" ];then
            if [ "$2" == "0" ] || [ "$2" == "1" ];then
               slapcat -n "$2"
	    else
               echo "el valor nomes pot ser 0 o 1"
	    fi
	else
	   slapcat
         fi
    ;;
  *)
        echo "No s'ha indicat un bon argument"
          ;;
esac

