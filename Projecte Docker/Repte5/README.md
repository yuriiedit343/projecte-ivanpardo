# Repte 5-IvanPardo

El primer pas seria crear un Dockerfile amb el següent contingut:

```Dockerfile
# Repte  Dockerfile
FROM debian:latest
LABEL version="1.0"
LABEL author="IvanPardo"
LABEL subject="Repte4"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/entrypoint.sh
ENTRYPOINT [ "/bin/bash" , "/opt/docker/entrypoint.sh" ]
WORKDIR /opt/docker
EXPOSE 389
#En comptes d'usar un cmd, haurem d'utilitzar un entrypoint, per fer que poguem utilitzar arguments
```
El segon pas es crear l'arxiu entrypoiny.sh amb un script que utilitzi els arguments rebuts per fer diferentes coses:
```bash
#!/bin/bash

if [ "$#" -eq 0 ];then
    option="start"
else
    option="$1"
fi
case "$option" in
  "slapd")
    # Crear un servidor LDAP vacío
    echo "Creando un servidor LDAP vacío..."
    rm -rf /var/lib/ldap/*
    rm -rf /etc/ldap/slapd.d/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "initdb")
    # Inicializar el servidor LDAP con datos de la organización
    echo "Inicializando el servidor LDAP con datos de la organización..."
    rm -rf /var/lib/ldap/*
    rm -rf /etc/ldap/slapd.d/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "start" | "edtorg" | "-z $1")
    # Iniciar el servidor LDAP utilizando la persistencia de datos de la base de datos y la configuración.
    echo "Iniciando el servidor LDAP utilizando la persistencia de datos existentes..."
    /usr/sbin/slapd -d0

    ;;
  "slapcat")
        if [ -n $2 ];then
          if [ $2== "0" ] || [ $2== "1" ];then
             slapcat -n "$2"
          else
             echo "el valor nomes pot ser 0 o 1"
          fi
        else
            slapcat
         fi

  *)
        echo "No s'ha indicat un bon argument"
          ;;
esac
```

creem l'arxiu slapd.conf que contindra la configuracio del slapd

```bash
#
# Veure slapd.conf(5) per detalls sobre les opcions de configuració.
# Aquest arxiu NO hauria de ser llegible per a tothom.
#
# Paquets Debian: slapd ldap-utils

include         /etc/ldap/schema/corba.schema
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/duaconf.schema
include         /etc/ldap/schema/dyngroup.schema
include         /etc/ldap/schema/inetorgperson.schema
include         /etc/ldap/schema/java.schema
include         /etc/ldap/schema/misc.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/openldap.schema
#include                /etc/ldap/schema/ppolicy.schema
include         /etc/ldap/schema/collective.schema

# Permetre connexions de clients LDAPv2. Això NO és la configuració per defecte.
allow bind_v2

pidfile         /var/run/slapd/slapd.pid
#argsfile       /var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la
# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```
*Despres tindriem que posar el fitxer amb totes les dades de la database a la nostra carpeta abans de fer l'imatge, en el meu cas l'arxiu es dira edt-org.ldif*
I creem la imatge amb la comanda:
```bash
docker build -t ldaprepte5:base .
```

Per iniciar la maquina sense dades seria:
```bash
docker run --rm --name repte5ivanpardo -h ldap.edt.org  -p 389:389  ldaprepte5:base sldap
```
Per iniciar la maquina amb dades seria:
```bash
docker run --rm --name repte5ivanpardo -h ldap.edt.org  -p 389:389  ldaprepte5:base initdb
```

Per iniciar la maquina amb les dades guardades seria:
```bash
docker run --rm --name ldap.edt.org -h ldap.edt.org -v ldap-dades:/var/lib/ldap -v ldap-config:/etc/ldap/slapd.d -p 389:389 -it a220877ipm14/repte5:latest  start
```

Per iniciar la maquina per fer el slapcat seria:
```bash
docker run --rm --name repte5ivanpardo -h ldap.edt.org  -p 389:389  ldaprepte5:base slapcat (0,1,res)
```
