### POSTGRES + ADMINER

## El primer pas es crear l'arxiu docker-compose.yml

```
version: '3.1'
services:
  db:
    image: ivanpardorepte3
    environment:
      POSTGRES_PASSWORD: jupiter
      POSTGRES_DB: training
    networks:
      - net23
  adminer:
    image: adminer
    restart: always
    ports:
        - 8080:8080
    networks:
        - net23
networks:
  net23:

```

## Executar el compose:

```
docker-compose -f docker-compose.yml up -d
```

## Comprobar que funcioni:
per comprobar el adminer hem d'obrir el navegador web i posar localhost:8080
un cop dins, especifiquem base de dades postgres i usuari
username: postgres
Password: (jupiter)

## Pausar el docker compose
```
docker-compose -f docker-compose.yml down
```


