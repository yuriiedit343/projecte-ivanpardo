### NET + PORTAINER

## El primer pas es crear l'arxiu docker-compose-net.yml

```
version: "3"
services:
    web:
        image: ivanpardo/repte7net
        container_name: net.edt.org
        hostname: net.edt.org
        ports:
            - "7:7"
            - "13:13"
            - "19:19"
        networks:
            - net23
    portainer:
        image: portainer/portainer
        ports:
            - "9000:9000"
    volumes:
        - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
    - net23
networks:
net23:
```

## Executar el compose:

```
docker-compose -f docker-compose-net.yml up -d
```

## Comprobar que funcioni:
per comprobar el net el que hem de fer es fer un nc del port del servici que vulguem probar,
per comprobar el portainer hem d'obrir el navegador web i posar localhost:9000
un cop dins, creem l'usuari admin
username: admin
Password: (contrasenya (en el meu cas es la del mail))

## Pausar el docker compose
```
docker-compose -f docker-compose-net.yml down
```


