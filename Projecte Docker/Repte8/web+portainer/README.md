### WEB + PORTAINER

## El primer pas es crear l'arxiu docker-compose.yml

```
version: "3"
services:
    web:
        image: ivanpardo/repte2
        container_name: web.edt.org
        hostname: web.edt.org
        ports:
            - "80:80"
        networks:
            - net23
    portainer:
        image: portainer/portainer
        ports:
            - "9000:9000"
    volumes:
        - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
    - net23
networks:
net23:
```

## Executar el compose:

```
docker-compose -f docker-compose.yml up -d
```

## Comprobar que funcioni:
per comprobar l'apache el que hem de fer es accedi al navegador web i posar localhost
per comprobar el portainer hem d'obrir el navegador web i posar localhost:9000
un cop dins, creem l'usuari admin
username: admin
Password: (contrasenya (en el meu cas es la del mail))

## Pausar el docker compose
```
docker-compose -f docker-compose.yml down
```


