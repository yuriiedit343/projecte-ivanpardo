### Postgres PHP

## El primer pas es crear l'arxiu docker-compose.yml

```
version= "3.1"
services:
  ldap:
    image: ivanpardo/repte4ldap:base
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - net23
  phpldapadmin:
    image: ivanpardophp
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - net23
networks:
 net23:

```

## Executar el compose:

```
docker-compose -f docker-compose.yml up -d
```

## Comprobar que funcioni:
per comprobar el adminer hem d'obrir el navegador web i posar localhost:80/phpldapadmin
un cop dins, especifiquem les dades necesaries
dn: cn=Manager,dc=edt,dc=org
Password: (secret)

## Pausar el docker compose
```
docker-compose -f docker-compose.yml down
```


